﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raysharp.Scripts.Utils
{
    class Time
    {

        static public double deltaTime { get { return (DateTime.Now - prevFrameDate).TotalSeconds; } }
        static public double time { get { return (DateTime.Now - firstFrameDate).TotalSeconds; } }

        static private DateTime firstFrameDate;
        static private DateTime prevFrameDate;

        static public void Set ()
        {
            firstFrameDate = DateTime.Now;
            prevFrameDate = firstFrameDate;

            Program.onFrameRenderer += Update;
        }

        static public void Update ()
        {
            prevFrameDate = DateTime.Now;
        }

    }
}
