﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raysharp.Scripts.Utils {
    struct Segment {
        public Vector2 a;
        public Vector2 b;

        public Vector2 normal { get; }

        public Segment (Vector2 pointA, Vector2 pointB) {
            a = pointA;
            b = pointB;

            Vector2 dir = b - a;
            this.normal = new Vector2(dir.y, -dir.x);
        }
    }

    class Polygon {


        private List<Vector2> vertices;
        private List<Segment> edges;

        public Polygon(List<Vector2> points) {
            vertices = points;
            edges = new List<Segment>();

            Segment edge;
            for (int i = 0; i < points.Count; i++) {
                int nextIndex = (i+1) % points.Count;

                edge = new Segment(points[i], points[nextIndex]);
                edges.Add(edge);
            }
        }

        public List<Segment> GetEdges () {
            return edges;
        }

    }
}
