﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raysharp.Scripts.Utils {
    class Vector2 {
        #region [CONSTANTS]
        public static Vector2 up { get { return new Vector2(0, 1); } }
        public static Vector2 down { get { return new Vector2(0, -1); } }
        public static Vector2 left { get { return new Vector2(-1, 0); } }
        public static Vector2 right { get { return new Vector2(1, 0); } }
        public static Vector2 zero { get { return new Vector2(0, 0); } }
        public static Vector2 one { get { return new Vector2(1, 1); } }
        #endregion

        public double x, y;


        public Vector2 () {
            x = 0;
            y = 0;
        }
        public Vector2 (double x, double y) {
            this.x = x;
            this.y = y;
        }


        #region [INSTANCE_METHODS]
        public void Normalize () {
            double norm = Length();

            x /= norm;
            y /= norm;
        }

        public Vector2 Normalized () {
            double norm = Length();

            return new Vector2(x / norm, y / norm);
        }

        public double Length () {
            return Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
        }
        #endregion


        #region [STATIC_METHODS]
        public static Vector2 Normalize (Vector2 v) {
            double norm = v.Length();
            return new Vector2(v.x / norm, v.y / norm);
        }

        public static double Distance (Vector2 v1, Vector2 v2) {
            return Math.Sqrt(Math.Pow(v1.x - v2.x, 2) + Math.Pow(v1.y - v2.y, 2));
        }
        #endregion


        #region [OPERATORS]
        public static Vector2 operator +(Vector2 v1, Vector2 v2) {
            return new Vector2(v1.x + v2.x, v1.y + v2.y);
        }

        public static Vector2 operator - (Vector2 v1, Vector2 v2) {
            return new Vector2(v1.x - v2.x, v1.y - v2.y);
        }

        public static Vector2 operator * (Vector2 v, double n) {
            return new Vector2(v.x * n, v.y * n);
        }

        public override string ToString () {
            return string.Format("({0}, {1})", x, y);
        }
        #endregion
    }
}