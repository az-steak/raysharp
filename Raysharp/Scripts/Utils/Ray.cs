﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raysharp.Scripts.Utils {
    class Ray {
        private Vector2 _direction;

        public Vector2 position;
        public Vector2 direction { get { return _direction; } set { 
            if (value.Length() == 0) {
                Console.WriteLine("ERROR : Can't set Ray direction to zero vector");
                return;
            }

            _direction = value;
        } }

        public Ray () {
            position = Vector2.zero;
            direction = Vector2.one;
        }

        public Ray (Vector2 position, Vector2 direction) {
            this.position = position;
            this.direction = direction;
        }
    }
}
