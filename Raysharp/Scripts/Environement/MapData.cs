﻿using Raysharp.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raysharp.Scripts.Environement {
    class MapData
    {
        public List<Wall> walls { get; private set; }

        public MapData () {
            Console.WriteLine("MOCK MAP DATA : TO BE REPLACED");

            walls = new List<Wall>();

            Polygon testPoly = new Polygon(new List<Vector2>() {
                new Vector2(-1, 0),
                new Vector2(-2, 1),
                new Vector2(-1, 2),
                new Vector2(1, 2),
                new Vector2(2, 0),
                new Vector2(1, -2),
                new Vector2(-1, -2),
                new Vector2(-2, -1)
            });

            walls = new List<Wall>() {
                new Wall(testPoly)
            };
        }
    }
}
