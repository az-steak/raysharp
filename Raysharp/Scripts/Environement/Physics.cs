﻿using Raysharp.Scripts.Utils;
using System;
using System.Collections.Generic;

namespace Raysharp.Scripts.Environement
{
    class Physics
    {

        public class RaycastHit {
            public Vector2 point;
            public Vector2 normal;
        }

        private static World world;

        public static void SetWorld (World loadedWorld) {
            world = loadedWorld;
        }

        public static bool Raycast (Vector2 origin, Vector2 direction) {
            Ray ray = new Ray(origin, direction);

            return RayIntersectSegments(ray, world.mapData);
        }

        #region [RAYCAST_FUNCTIONS]
        public static bool RayIntersectSegments (Ray ray, List<Segment> segments, out RaycastHit hit) {
            bool hitted = false;
            bool lastHitted = false;
            hit = null;

            double nearestDistance = Double.MaxValue;
            RaycastHit lastHit = null;

            foreach (Segment segment in segments) {
                lastHitted = RayIntersectSegment(ray, segment, out lastHit);
                hitted = hitted || lastHitted;

                if (lastHitted) {
                    double distance = Vector2.Distance(ray.position, lastHit.point);

                    if (distance < nearestDistance) {
                        nearestDistance = distance;
                        hit = lastHit;
                    }
                }
            }

            return hitted;
        }
        private static bool RayIntersectSegment (Ray ray, Segment segment, out RaycastHit hit) {
            double x1 = segment.a.x;
            double y1 = segment.a.y;
            double x2 = segment.b.x;
            double y2 = segment.b.y;

            double x3 = ray.position.x;
            double y3 = ray.position.y;
            double x4 = ray.position.x + ray.direction.x;
            double y4 = ray.position.y + ray.direction.y;

            double denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
            hit = null;

            if (denominator == 0) return false;

            double t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / denominator;
            double u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / denominator;

            if (0 < t && t < 1 && 0 < u) {
                hit = new RaycastHit();
                double x = x1 + t * (x2 - x1);
                double y = y1 + t * (y2 - y1);
                hit.point = new Vector2(x, y);

                return true;
            }


            return false;
        }
        #endregion
    }
}
