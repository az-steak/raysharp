﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raysharp.Scripts.Entities {
    class Camera : Entity {

        private double _fov;
        private double _radFov;

        public double fov { get { return _fov; } set {
                _fov = value;
                _radFov = value * Utils.AdvMath.DEG2RAD;
            } }
        public double radFov { get { return _radFov; } set {
                _radFov = value;
                _fov = value * Utils.AdvMath.RAD2DEG;
            } }

        public Camera () {
            position = new Utils.Vector2(-3, 1);
        }

    }
}
