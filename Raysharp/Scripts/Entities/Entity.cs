﻿using Raysharp.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raysharp.Scripts.Entities {
    class Entity {

        public Vector2 position = Vector2.zero;
        public double orientation = 0;
        public double height = 0;

        public Vector2 forward { get { return new Vector2(Math.Cos(orientation), Math.Sin(orientation)); } }
        public Vector2 right { get { return new Vector2(Math.Cos(orientation + Math.PI / 2), Math.Sin(orientation + Math.PI / 2)); } }

    }
}
