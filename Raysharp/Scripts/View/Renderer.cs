﻿using Raysharp.Scripts.Entities;
using Raysharp.Scripts.Environement;
using Raysharp.Scripts.Utils;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using static Raysharp.Scripts.Environement.Physics;

namespace Raysharp.Scripts.View {
    class Renderer {

        private RenderWindow window;
        private World world;


        #region [RENDER_SETTINGS]
        private int columnResolution = 256;
        #endregion

        public Renderer (RenderWindow renderWindow) {
            window = renderWindow;
        }

        public void SetMapData (World worldData) {
            world = worldData;
        }

        public void RenderFrame (Camera camera) {
            window.Clear();

            RenderWalls(camera);

            window.Display();
        }


        #region [RENDER_FUNCTIONS]
        private void RenderWalls (Camera camera) {
            double fovInterval = camera.radFov / columnResolution;
            Vector2 cameraForward = camera.forward;
            Vector2 raycastDirection = Vector2.zero;

            for (int x = 0; x < columnResolution; x++) {
                double angleFromCenter = x * fovInterval - camera.radFov / 2;

                raycastDirection.x = cameraForward.x + Math.Cos(angleFromCenter);
                raycastDirection.y = cameraForward.y + Math.Sin(angleFromCenter);

                if (true) {

                }
            }
        }


        private void RenderWallColumn (RaycastHit hit, Color color) {

        }


        private void DebugDrawLine (Segment segment) {
            VertexArray line = new VertexArray(PrimitiveType.Lines);
            Vertex a = new Vertex(new Vector2f((float)segment.a.x, (float)segment.a.y));
            Vertex b = new Vertex(new Vector2f((float)segment.b.x, (float)segment.b.y));

            line.Append(a);
            line.Append(b);

            window.Draw(line);
        }
        #endregion
    }
}
