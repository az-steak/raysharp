﻿using Raysharp.Scripts.Entities;
using Raysharp.Scripts.Environement;
using Raysharp.Scripts.Utils;
using Raysharp.Scripts.View;
using SFML.Graphics;
using SFML.Window;
using System;

namespace Raysharp {
    class Program {
        static private RenderWindow window;
        static private Renderer renderer;


        static private Camera debugCamera;

        static public Action onFrameRenderer;

        static void Main (string[] args) {
            Time.Set();
            window = new RenderWindow(VideoMode.DesktopMode, "Raycast", Styles.Default);
            renderer = new Renderer(window);

            World loadedWorld = new World();
            debugCamera = new Camera();

            SetWorld(loadedWorld);

            window.SetActive(true);

            while (window.IsOpen) {
                renderer.RenderFrame(debugCamera);
                onFrameRenderer?.Invoke();
            }
        }

        static private void SetWorld (World world) {
            renderer.SetMapData(world);
            
        }
    }
}
